function pdAnimate(){
    var that = this;
    var controller;
    var path = document.querySelector('.path');
    var lineLength = 0,
        durationObj = [];

    var path = $('#path_sp');
    
    var sceneLine1, sceneLine2, sceneLine3, sceneLine4, sceneLine5, sceneLine6, sceneLine7;

    var tlObj1 = {
        lineLength: 2963.976318359375,
        curentlineLength: 2963.976318359375,
        tolineLength: 2200,
        time: 3
    },
    tlObj2 = {
        lineLength: 2200,
        curentlineLength: 2200,
        tolineLength: 800,
        time: 3
    },
    tlObj3 = {
        lineLength: 800,
        curentlineLength: 800,
        tolineLength: 0,
        time: 3
    };

    var ele_trigger_0 = '.warp-line',
        ele_trigger_1 = '.warp-line .box-1',
        ele_trigger_2 = '.warp-line .box-2',
        ele_trigger_3 = '.warp-line .box-3',
        ele_trigger_4 = '.warp-line .box-4';

    var ele_1 = '.warp-line .box-1',
        ele_2 = '.warp-line .box-2',
        ele_3 = '.warp-line .box-3',
        ele_4 = '.warp-line .box-4';

    var obj_ant = [
        {opacity: 0, y: 50},
        {opacity: 1, y: 0, ease: Sine.easeOut},
        {opacity: 0, x: 50},
        {opacity: 1, x: 0, ease: Sine.easeOut}
    ];

    this.animateLine = function(){
        sceneLine1 = new ScrollMagic.Scene({triggerElement: ele_trigger_0, duration: 100})
            .addTo(controller)
            .on("progress", function(e){
                that.tweenPath(tlObj1, e.progress.toFixed(3));
            });

        sceneLine2 = new ScrollMagic.Scene({triggerElement: ele_trigger_1, duration: 100})
            .addTo(controller)
            .on("progress", function(e){
                that.tweenPath(tlObj2, e.progress.toFixed(3));
            });

        sceneLine3 = new ScrollMagic.Scene({triggerElement: ele_trigger_2, duration: 100})
            .addTo(controller)
            .on("progress", function(e){
                that.tweenPath(tlObj3, e.progress.toFixed(3));
            });

        that.updateDuration();
    }

    this.animateEle = function(){
        var timeline1 = new TimelineMax()
            .fromTo('.warp-line .box-1 .tt', 0.5,obj_ant[0], obj_ant[1])
            .fromTo('.warp-line .box-1 .text', 0.5,obj_ant[0], obj_ant[1]);
        new ScrollMagic.Scene({triggerElement: ele_trigger_1, offset: -50})
            .setTween(timeline1)
            .addTo(controller);

        var timeline2 = new TimelineMax()
            .fromTo('.warp-line .box-2 .tt', 0.5,obj_ant[0], obj_ant[1])
            .fromTo('.warp-line .box-2 .text', 0.5,obj_ant[0], obj_ant[1]);
        new ScrollMagic.Scene({triggerElement: ele_trigger_2, offset: -50})
            .setTween(timeline2)
            .addTo(controller);

        var timeline3 = new TimelineMax()
            .fromTo('.warp-line .box-3 .tt', 0.5,obj_ant[0], obj_ant[1])
            .fromTo('.warp-line .box-3 .text', 0.5,obj_ant[0], obj_ant[1]);
        new ScrollMagic.Scene({triggerElement: ele_trigger_3, offset: -150})
            .setTween(timeline3)
            .addTo(controller);

        var timeline4 = new TimelineMax()
            .add(TweenMax.fromTo(ele_4, 1,obj_ant[0], obj_ant[1]));
        new ScrollMagic.Scene({triggerElement: ele_trigger_4, offset: -20})
            .setTween(timeline4)
            .addTo(controller);


        // get all slides
        var slides = document.querySelectorAll(".show-all");

        // create scene for every slide
        for (var i=0; i<slides.length; i++) {
            new ScrollMagic.Scene({
                    triggerElement: slides[i]
                })
                .setTween(TweenMax.fromTo(slides[i], 0.5,obj_ant[0], obj_ant[1]))
                .addTo(controller);
        }

        // get all slides
        var tw_sp_img_4 = new TimelineMax()
            .from('.img-4', 2,{x: 2000, ease: Power4.easeOut})
            .staggerFrom('.banner-product .box', 0.5, {opacity: 0, y: 80,  ease: Power2.easeOut}, 0.2, "-=1.2");

        new ScrollMagic.Scene({
            triggerElement: '.img-4'
        })
            .setTween(tw_sp_img_4)
            .addTo(controller);

        // get all slides
        var tw_sp_img_8 = TweenMax.from('.img-8 img', 1,{x: 1000, ease: Power4.easeOut});

        new ScrollMagic.Scene({
            triggerElement: '.img-8'
        })
            .setTween(tw_sp_img_8)
            .reverse(false)
            .addTo(controller);
    }

    this.calcDurationObj = function(){
        var top = $(ele_trigger_0).offset().top;
        var oset1 = $(ele_trigger_1).offset().top;
        var oset2 = $(ele_trigger_2).offset().top;
        var oset3 = $(ele_trigger_3).offset().top;

        durationObj[0] =  oset1 - top;
        durationObj[1] =  oset2 - oset1;
        durationObj[2] =  oset3 - oset2;
    }

    this.updateDuration = function(){
        that.calcDurationObj();
        sceneLine1.duration(durationObj[0]);
        sceneLine2.duration(durationObj[1]);
        sceneLine3.duration(durationObj[2]);
        // sceneLine4.duration(durationObj[3]);
    }

    this.tweenPath = function(obj, progress){
        var s = obj.curentlineLength - progress * (obj.curentlineLength - obj.tolineLength);
        TweenMax.killTweensOf(obj);
        TweenMax.to(obj, 1, {lineLength: s, ease: Cubic.easeOut,
            onUpdate:function(){
                path.css({"stroke-dashoffset": obj.lineLength});
            }
        });
    };

    this.init = function(){
        controller = new ScrollMagic.Controller();
        lineLength = path[0].getTotalLength();
        path.css({"stroke-dasharray": lineLength, "stroke-dashoffset": lineLength});
        that.animateLine();
        that.animateEle();
    }
}

var pdAnimate = new pdAnimate();
$(document).ready(function(){
    pdAnimate.init();
});