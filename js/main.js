$.fn.serializeObject = function () {

    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

var isMobile = (function(a){return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);
function PageFunction(){
    var self = this;
    var ww = wh = 0;
    this.init = function(){
        var controller = new ScrollMagic.Controller();

        var tween = TweenMax.to(".scroll-bar", 0.5, {opacity: 0});

        var scene = new ScrollMagic.Scene({
            triggerElement: ".warp-svg",
            triggerHook: 0.5
        })
            .setTween(tween)
            .addTo(controller);

        $(document).on('click','.main-header ul.normal li', function () {
            $(this).addClass('active');
            $('.main-header ul.normal li').removeClass('active');
        });

        $(document).on('click','.icon-menu', function () {
            $(this).toggleClass('open');
            $('.main-header ul.mb').toggleClass('open');
            return false;
        });

        $(document).on('click', '.main-header ul.mb > li > a', function(){
            $('html, body').animate({
                scrollTop: $( $.attr(this, 'href') ).offset().top
            }, 500);
            return false;
        });

        $(document).on('click', '.main-header ul.normal li a', function(){
            $('.main-header ul.normal li').removeClass('active');
            $(this).parent().addClass('active');
            $('html, body').animate({
                scrollTop: $( $.attr(this, 'href') ).offset().top
            }, 500);
            $('.icon-menu').removeClass('open');
            $('.main-header ul.normal').removeClass('open');
            return false;
        });


        scroll_menu();
    };

    this.resizeWindow = function(){
        ww = $(window).width();
        wh = $(window).height();
        $(window).resize(function(){
            var w_that = $(this);
            ww = w_that.width();
            wh = w_that.height();
        });
    };

    this.share_fb = function(){
        console.log('aaaa');
    }

    this.homepage = function () {
        var swiper = new Swiper('.sec3 .swiper-container', {
            slidesPerView: 1,
            paginationClickable: true,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            paginationClass:'swiper-pagination-switch',
            paginationActiveClass:'active',
            spaceBetween: 20,
            onSlideChangeEnd: function() {
                var index = swiper.activeIndex;
                $('.pagination-dot li').removeClass('active');
                $('.pagination-dot li').eq(index).addClass('active');
                return false;
            }
        });

        $(document).on('click','.sec3 .pagination-dot li',function () {
            swiper.slideTo($(this).index());
            $('.sec3 .pagination-dot li').removeClass('active');
            $(this).addClass('active');
            return false;
        });


        $(document).on('click','.sec-header .wrap-vd',function (e) {
            $("#video-th")[0].src += "?autoplay=1";
            $('.sec-header .wrap-vd .bg').fadeOut();
            $('.sec-header .wrap-vd .ic-play').fadeOut();
            e.preventDefault();
        });

        $(document).on('click','.answer .close',function () {
            $('.answer').removeClass('show-up');
            $('.sec3 ul.normal').removeClass('stop-click');
            $('.swiper-btn').removeClass('stop-click');
        });

        $(document).on('click','.item-slide',function () {
            var ele = $(this);

            var question = ele.data('question');
            var answer = ele.data('answer');

           $('.answer .tt').html(question);
            $('.answer .des').html(answer);

            $('.answer').addClass('show-up');
            $('.sec3 ul.normal').addClass('stop-click');
            $('.swiper-btn').addClass('stop-click');
        });

        var controller = new ScrollMagic.Controller();

        var tween = TweenMax.to(".ov", 0.5, {opacity: 0.7});

        var scene = new ScrollMagic.Scene({
            triggerElement: "#ani",
            triggerHook: 0.8
        })
            .setTween(tween)
            .addTo(controller);

        var scitem = document.querySelectorAll(".js-wrap-item-ant");
        for (var i=0; i<scitem.length; i++) {
            var tween_ant = TweenMax.staggerFrom(scitem[i].getElementsByClassName('js-item-ant'), 0.5, {opacity: 0, y: 100}, 0.3);

            new ScrollMagic.Scene({
                triggerElement: scitem[i]
            })
                .setTween(tween_ant)
                .addTo(controller)
                .addTo(controller)
                .reverse(false);
        }

        var slitem = document.querySelectorAll('.js-sl-item');

        for ( var j=0 ; j<slitem.length; j++) {
            var tween_an =  new TimelineMax()
                .staggerFrom(slitem[j].getElementsByClassName('js-sl-ant'), 0.5, {opacity: 0, x: -100}, 0.3)
                .staggerFrom(slitem[j].getElementsByClassName('js-jut'), 0.5, {scale: 0, ease: Back.easeInOut}, 0.3)
                .staggerFrom(slitem[j].getElementsByClassName('js-text-zoom'), 0.5, {scale: 0, ease: Back.easeInOut}, 0.3);

            new ScrollMagic.Scene({
                triggerElement: slitem[j]
            })
                .setTween(tween_an)
                .addTo(controller)
                .reverse(false);
        }

        if(isMobile) {
            $(document).on('click','.item-slide',function () {
                $('html,body').animate({
                    scrollTop: $('.sec3-swiper').offset().top
                }, 500);
                return false;
            })
        }

        $( "#contact" ).validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                names: {
                    required: true,
                    minlength: 2
                },
                question: {
                    required: true,
                    minlength: 2
                }
            },
            messages: {
                names: {
                    required: "Không được để trống",
                    minlength: "Nhập ít nhất 2 ký tự"
                },
                email: {
                    required: "Không được để trống",
                    email: "Email của bạn phải điền đúng theo mẫu name@domain.com"
                },
                question: {
                    required: "Không được để trống",
                    minlength: "Nhập ít nhất 2 ký tự"
                }
            }, submitHandler: function (form, e) {
                e.preventDefault();
                //  $('#contact').LoadingOverlay("show");
                var data = $('#contact').serializeObject();
                $.ajax({
                    url: ajaxurl,
                    dataType: 'json',
                    type: 'post',
                    data: {
                        action: 'add_question',
                        data: data,

                    },
                    success: function (reponse) {

                        // $('#contact').LoadingOverlay("hide");
                        if (reponse.error != true) {

                            $('#contact').find("input[type=text], textarea").val("");
                            swal({


                                title: 'Chúc mừng',
                                html: 'Bạn đã gởi câu hỏi thành công.',
                                type: 'success',
                                confirmButtonText: 'Đóng lại',
                                // confirmButtonColor: '#66c328',
                                allowOutsideClick: false
                            });
                            ;

                        } else {
                            swal({
                                title: 'Thông báo!',
                                html: 'Đã có lỗi xãy ra khi gởi câu hỏi. Xin vui lòng thử lại',
                                type: 'error',
                                allowOutsideClick: false
                            });


                        }
                    }
                })
            }

        });



    }
    this.cgtkk = function () {
        var controller = new ScrollMagic.Controller();

        var slitem = document.querySelectorAll('.js-sl-item');

        for ( var j=0 ; j<slitem.length; j++) {
            var tween_an =  new TimelineMax()
                .staggerFrom(slitem[j].getElementsByClassName('js-sl-ant1'), 0.5, {opacity: 0, x: -100}, 0.3)
                .staggerFrom(slitem[j].getElementsByClassName('js-sl-ant2'), 0.5, {opacity: 0, x: -100}, 0.3)
                .staggerFrom(slitem[j].getElementsByClassName('js-sl-ant3'), 0.5, {opacity: 0, x: -100}, 0.3)
                .staggerFrom(slitem[j].getElementsByClassName('js-sl-ant4'), 0.5, {opacity: 0, y: -100}, 0.3)
                .staggerFrom(slitem[j].getElementsByClassName('js-sl-ant5'), 0.5, {opacity: 0, y: -100}, 0.3)
                .staggerFrom(slitem[j].getElementsByClassName('js-sl-ant6'), 0.5, {opacity: 0, x: 100}, 0.3);

            new ScrollMagic.Scene({
                triggerElement: slitem[j]
            })
                .setTween(tween_an)
                .addTo(controller)
                .reverse(false);
        }
    }

    this.tamnhin1 = function () {
        var controller = new ScrollMagic.Controller();
        var slitem = document.querySelectorAll('.js-sl-item');
        if(isMobile) {
            for ( var j=0 ; j<slitem.length; j++) {
                var tween_an =  new TimelineMax()
                    .staggerFrom(slitem[j].getElementsByClassName('js-ico-zoom1'), 0.5, {scale: 0, ease: Back.easeInOut}, 0.3)
                    .staggerFrom(slitem[j].getElementsByClassName('js-text-sl1'), 0.5, {opacity: 0, x: -100}, 1)
                    .staggerFrom(slitem[j].getElementsByClassName('js-ico-zoom2'), 0.5, {scale: 0, ease: Back.easeInOut}, 0.3)
                    .staggerFrom(slitem[j].getElementsByClassName('js-text-sl2'), 0.5, {opacity: 0, x: -100}, 1)
                    .staggerFrom(slitem[j].getElementsByClassName('js-ico-zoom3'), 0.5, {scale: 0, ease: Back.easeInOut}, 0.3)
                    .staggerFrom(slitem[j].getElementsByClassName('js-text-sl3'), 0.5, {opacity: 0, x: -100}, 1)
                    .staggerFrom(slitem[j].getElementsByClassName('js-item-ant'), 0.5, {opacity: 0, y: 100}, 0.3);

                new ScrollMagic.Scene({
                    triggerElement: slitem[j]
                })
                    .setTween(tween_an)
                    .addTo(controller)
                    .reverse(false);
            }
        }else{
            for ( var j=0 ; j<slitem.length; j++) {
                var tween_an =  new TimelineMax()
                    .staggerFrom(slitem[j].getElementsByClassName('js-item-ant'), 0.5, {opacity: 0, y: 100}, 0.3)
                    .staggerFrom(slitem[j].getElementsByClassName('js-text-zoom'), 0.5, {scale: 0, ease: Back.easeInOut}, 0.3)
                    .staggerFrom(slitem[j].getElementsByClassName('js-sl-ant'), 0.5, {opacity: 0, x: -100}, 0.3)
                    .staggerFrom(slitem[j].getElementsByClassName('js-jut'), 0.5, {scale: 0, ease: Back.easeInOut}, 0.3)
                    .staggerFrom(slitem[j].getElementsByClassName('js-text-sl'), 0.5, {opacity: 0, x: -100}, 1)
                    .staggerFrom(slitem[j].getElementsByClassName('js-ico-zoom1'), 0.5, {scale: 0, ease: Back.easeInOut}, 0.3)
                    .staggerFrom(slitem[j].getElementsByClassName('js-text-sl1'), 0.5, {opacity: 0, x: -100}, 1)
                    .staggerFrom(slitem[j].getElementsByClassName('js-ico-zoom2'), 0.5, {scale: 0, ease: Back.easeInOut}, 0.3)
                    .staggerFrom(slitem[j].getElementsByClassName('js-text-sl2'), 0.5, {opacity: 0, x: -100}, 1)
                    .staggerFrom(slitem[j].getElementsByClassName('js-ico-zoom3'), 0.5, {scale: 0, ease: Back.easeInOut}, 0.3)
                    .staggerFrom(slitem[j].getElementsByClassName('js-text-sl3'), 0.5, {opacity: 0, x: -100}, 1);

                new ScrollMagic.Scene({
                    triggerElement: slitem[j]
                })
                    .setTween(tween_an)
                    .addTo(controller)
                    .reverse(false);
            }
        }
    }
    this.tamnhin2 = function () {
        var controller = new ScrollMagic.Controller();

        var slitem = document.querySelectorAll('.js-sl-item');

        for ( var j=0 ; j<slitem.length; j++) {
            var tween_an =  new TimelineMax()
                .staggerFrom(slitem[j].getElementsByClassName('js-item-ant'), 0.5, {opacity: 0, y: 100}, 0.3)
                .staggerFrom(slitem[j].getElementsByClassName('js-text-zoom'), 0.5, {scale: 0, ease: Back.easeInOut}, 0.3)
                .staggerFrom(slitem[j].getElementsByClassName('js-sl-ant'), 0.5, {opacity: 0, x: -100}, 0.3)
                .staggerFrom(slitem[j].getElementsByClassName('js-jut'), 0.5, {scale: 0, ease: Back.easeInOut}, 0.3)
                .staggerFrom(slitem[j].getElementsByClassName('js-text-sl'), 0.5, {opacity: 0, x: -100}, 1)
                .staggerFrom(slitem[j].getElementsByClassName('js-ico-zoom1'), 0.5, {scale: 0, ease: Back.easeInOut}, 0.3)
                .staggerFrom(slitem[j].getElementsByClassName('js-text-sl1'), 0.5, {opacity: 0, x: -100}, 1)
                .staggerFrom(slitem[j].getElementsByClassName('js-ico-zoom2'), 0.5, {scale: 0, ease: Back.easeInOut}, 0.3)
                .staggerFrom(slitem[j].getElementsByClassName('js-text-sl2'), 0.5, {opacity: 0, x: -100}, 1)
                .staggerFrom(slitem[j].getElementsByClassName('js-ico-zoom3'), 0.5, {scale: 0, ease: Back.easeInOut}, 0.3)
                .staggerFrom(slitem[j].getElementsByClassName('js-text-sl3'), 0.5, {opacity: 0, x: -100}, 1);

            new ScrollMagic.Scene({
                triggerElement: slitem[j]
            })
                .setTween(tween_an)
                .addTo(controller)
                .reverse(false);
        }
    }

    this.tamnhinvssm = function () {
        var controller = new ScrollMagic.Controller();

        var tween = TweenMax.to(".ov", 0.5, {opacity: 0.7});


        var scitem = document.querySelectorAll(".js-wrap-item-ant");
        for (var i=0; i<scitem.length; i++) {
            var tween_ant =  new TimelineMax()
                .staggerFrom(scitem[i].getElementsByClassName('js-item-ant1'), 0.5, {opacity: 0, y: 100}, 0.3)
                .staggerFrom(scitem[i].getElementsByClassName('js-item-ant2'), 0.5, {opacity: 0, y: 100}, 0.3)
                .staggerFrom(scitem[i].getElementsByClassName('js-item-ant3'), 0.5, {opacity: 0, y: 100}, 0.3)
                .staggerFrom(scitem[i].getElementsByClassName('js-item-ant4'), 0.5, {opacity: 0, y: 100}, 0.3);

            new ScrollMagic.Scene({
                triggerElement: scitem[i]
            })
                .setTween(tween_ant)
                .addTo(controller)
                .addTo(controller)
                .reverse(false);
        }
    }

    this.resizeWindow();

    this.tab_page = function () {
        $( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
        $( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );

        var swiper = new Swiper('.swiper-mb .swiper-container', {
            slidesPerView: 1,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            spaceBetween: 100
        });


        $( "#form-reg" ).validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                names: {
                    required: true,
                    minlength: 2
                },
                address: {
                    required: true,

                },
                fone: {
                    required: true
                }
            },
            messages: {
                names: {
                    required: "Không được để trống",
                    minlength: "Nhập ít nhất 2 ký tự"
                },
                email: {
                    required: "Không được để trống",
                    email: "Email của bạn phải điền đúng theo mẫu name@domain.com"
                },
                address: {
                    required: "Không được để trống",

                },
                fone: {
                    required: "Không được để trống"
                }

            }, submitHandler: function (form, e) {
                e.preventDefault();


                var check = $("#input-check-rules").prop("checked");
                if(!check){
                    swal({
                        title: 'Thông báo!',
                        html: 'Bạn chưa đánh dấu "Tôi đồng ý với thể lệ và điều kiện của chương trình."',
                        type: 'error',
                        allowOutsideClick: false
                    });
                    return false;
                }

                //  $('#form-reg').LoadingOverlay("show");
                var data = $('#form-reg').serializeObject();
                $.ajax({
                    url: ajaxurl,
                    dataType: 'json',
                    type: 'post',
                    data: {
                        action: 'add_contact',
                        data: data,

                    },




                    success: function (reponse) {

                        // $('#form-reg').LoadingOverlay("hide");
                        if (reponse.error != true) {

                            $('#form-reg').find("input[type=text], textarea").val("");
                            swal({
                                title: 'Chúc mừng',
                                html: 'Bạn đã đăng ký nhận mẫu thành công!',
                                type: 'success',
                                showCancelButton: true,
                                confirmButtonText: 'Chia sẽ lên Facebook',
                                cancelButtonText: 'Đóng lại',
                                // confirmButtonColor: '#66c328',
                                allowOutsideClick: false
                            }).then(function () {
                                FB.ui({
                                    method: 'share',
                                    href: baseurl + '/event-tang-qua/',
                                }, function(response){});
                            })
                            ;

                        } else {
                            swal({
                                title: 'Thông báo!',
                                html: 'Đã có lỗi xãy ra khi đăng ký. Xin vui lòng thử lại',
                                type: 'error',
                                allowOutsideClick: false
                            });


                        }
                    }
                })
            }
        });

        $( "#form-gift" ).validate({
            rules: {

                sender_name: {
                    required: true,
                    minlength: 2
                },
                sender_phone: {
                    required: true,
                    minlength: 2
                },
                reciver_name: {
                    required: true,
                    minlength: 2
                },
                reciver_phone: {
                    required: true,
                    minlength: 2
                },
                reciver_address: {
                    required: true,

                },
                reciver_city: {
                    required: true
                }
            },
            messages: {
                sender_name: {
                    required: "Không được để trống",
                    minlength: "Nhập ít nhất 2 ký tự"
                },
                sender_phone: {
                    required: "Không được để trống",
                    minlength: "Nhập ít nhất 2 ký tự"
                },
                reciver_name: {
                    required: "Không được để trống",
                    minlength: "Nhập ít nhất 2 ký tự"
                },
                reciver_phone: {
                    required: "Không được để trống",
                    minlength: "Nhập ít nhất 2 ký tự"
                },

                reciver_address: {
                    required: "Không được để trống",

                },
                reciver_city: {
                    required: "Không được để trống"
                }

            }, submitHandler: function (form, e) {
                e.preventDefault();


                var check = $("#input-check-rules").prop("checked");
                if(!check){
                    swal({
                        title: 'Thông báo!',
                        html: 'Bạn chưa đánh dấu "Tôi đồng ý với thể lệ và điều kiện của chương trình."',
                        type: 'error',
                        allowOutsideClick: false
                    });
                    return false;
                }

                //  $('#form-reg').LoadingOverlay("show");
                var data = $('#form-gift').serializeObject();
                $.ajax({
                    url: ajaxurl,
                    dataType: 'json',
                    type: 'post',
                    data: {
                        action: 'add_gift',
                        data: data,

                    },




                    success: function (reponse) {

                        // $('#form-reg').LoadingOverlay("hide");
                        if (reponse.error != true) {

                            $('#form-gift').find("input[type=text], textarea").val("");
                            swal({
                                title: 'Chúc mừng bạn đã đăng ký tặng mẫu thành công!',
                                html: 'Hàng tuần, 100 phần quà sẽ được gửi ngẫu nhiên đến các khách hàng may mắn.Danh sách khách hàng may mắn được công bố trên Facebook Fanpage TH true Herbal vào thứ 5 hàng tuần.!',
                                type: 'success',
                                showCancelButton: true,
                                confirmButtonText: 'Chia sẽ lên Facebook',
                                cancelButtonText: 'Đóng lại',
                                // confirmButtonColor: '#66c328',
                                allowOutsideClick: false
                            }).then(function () {
                                FB.ui({
                                    method: 'share',
                                    href: baseurl + '/suc-song/',
                                }, function(response){});
                            })
                            ;

                        } else {
                            swal({
                                title: 'Thông báo!',
                                html: 'Đã có lỗi xãy ra khi đăng ký. Xin vui lòng thử lại',
                                type: 'error',
                                allowOutsideClick: false
                            });


                        }
                    }
                })
            }
        });

        $( "#form-time" ).validate({
            rules: {

                name: {
                    required: true,

                },
                phone: {
                    required: true,

                },
                position: {
                    required: true,

                },
                room: {
                    required: true,

                },
                company: {
                    required: true,

                },
                address: {
                    required: true
                },
                city: {
                    required: true
                },
                number: {
                    required: true
                },
                time: {
                    required: true
                },
                hour: {
                    required: true
                },
                min: {
                    required: true,

                },
                member: {
                    required: true,

                },
                waiter: {
                    required: true,

                },
                adv: {
                    required: true,

                }

            },
            messages: {

                name: {
                    required: "Không được để trống",

                },
                phone: {
                    required: "Không được để trống"
                },
                position: {
                    required: "Không được để trống",

                },
                room: {
                    required: "Không được để trống"
                },
                company: {
                    required: "Không được để trống",

                },
                address: {
                    required: "Không được để trống"
                },
                city: {
                    required: "Không được để trống",

                },
                number: {
                    required: "Không được để trống"
                },
                time: {
                    required: "Không được để trống",

                },
                hour: {
                    required: "Không được để trống",

                },
                min: {
                    required: "Không được để trống",

                },
                member: {
                    required: "Không được để trống",

                },
                waiter: {
                    required: "Không được để trống",

                },
                adv: {
                    required: "Không được để trống",

                },



            }, submitHandler: function (form, e) {
                e.preventDefault();


                var check = $("#input-check-rules").prop("checked");
                if(!check){
                    swal({
                        title: 'Thông báo!',
                        html: 'Bạn chưa đánh dấu "Tôi đồng ý với thể lệ và điều kiện của chương trình."',
                        type: 'error',
                        allowOutsideClick: false
                    });
                    return false;
                }

                //  $('#form-reg').LoadingOverlay("show");
                var data = $('#form-time').serializeObject();
                $.ajax({
                    url: ajaxurl,
                    dataType: 'json',
                    type: 'post',
                    data: {
                        action: 'add_time',
                        data: data,

                    },




                    success: function (reponse) {

                        // $('#form-reg').LoadingOverlay("hide");
                        if (reponse.error != true) {

                            $('#form-time').find("input[type=text],input[type=number], textarea,select").val("");
                            swal({
                                title: 'Chúc mừng bạn đã đăng ký Event Herbal Time thành công!',
                                html: '',
                                type: 'success',
                                showCancelButton: true,
                                confirmButtonText: 'Chia sẽ lên Facebook',
                                cancelButtonText: 'Đóng lại',
                                // confirmButtonColor: '#66c328',
                                allowOutsideClick: false
                            }).then(function () {
                                FB.ui({
                                    method: 'share',
                                    href: baseurl + '/herbal-time/',
                                }, function(response){});
                            })
                            ;

                        } else {
                            swal({
                                title: 'Thông báo!',
                                html: 'Đã có lỗi xãy ra khi đăng ký. Xin vui lòng thử lại',
                                type: 'error',
                                allowOutsideClick: false
                            });


                        }
                    }
                })
            }
        });

        $("[data-fancybox]").fancybox();

        var controller = new ScrollMagic.Controller();

        var tween = TweenMax.to(".ov", 0.5, {opacity: 0.7});

        var slitem = document.querySelectorAll('.js-sl-item');

        for ( var j=0 ; j<slitem.length; j++) {
            var tween_an =  new TimelineMax()
                .staggerFrom(slitem[j].getElementsByClassName('js-item-ant'), 0.5, {opacity: 0, y: 100}, 0.3)

            new ScrollMagic.Scene({
                triggerElement: slitem[j]
            })
                .setTween(tween_an)
                .addTo(controller)
                .reverse(false);
        }
    }

}

var PageFunction = new PageFunction();

$(document).ready(function(){
    PageFunction.init();
    $('[data-toggle="datepicker"]').datepicker();

    $('#datetimepicker4').datetimepicker({
        format: 'LT'
    });

    $('a.check_link').click(function(e){

        var ele = $(this);
        var href = ele.attr('src');
        if ($.trim(href) == '' || $.trim(href) == '#'  ){
            swal({
                title: 'Thông báo!',
                html: 'Nội dung đang được cập nhật. Vui lòng quay lại sau',
                type: 'warning',
                allowOutsideClick: false,
                confirmButtonText: 'Đóng lại',
            });
            e.preventDefault();
            return false;
        }
    });

});


function scroll_menu() {
    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = $('.main-header').outerHeight();

    $(window).scroll(function(event){
        didScroll = true;
    });

    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        var st = $(this).scrollTop();

        // Make sure they scroll more than delta
        if(Math.abs(lastScrollTop - st) <= delta)
            return;

        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if (st > lastScrollTop && st > navbarHeight){
            // Scroll Down
            $('.main-header').removeClass('nav-down').addClass('nav-up');
            $('.main-header ul.mb').removeClass('open');
            $('.main-header .icon-menu').removeClass('open');
        } else {
            // Scroll Up
            if(st + $(window).height() < $(document).height()) {
                $('.main-header ').removeClass('nav-up').addClass('nav-down');
            }
        }

        lastScrollTop = st;
    }
}

function myFunction() {
    alert('aaaa');
}